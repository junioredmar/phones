﻿using System.Collections.Generic;
using AndDigital.CodingChallenge.Data.Models;

namespace AndDigital.CodingChallenge.Data.Repository
{
    public class CustomerRepository : BaseRepository
    {
        public IEnumerable<Customer> GetAll()
        {
            return Customers;
        }
    }
}
