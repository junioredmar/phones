﻿using System.Collections.Generic;
using System.Linq;
using AndDigital.CodingChallenge.Data.Models;

namespace AndDigital.CodingChallenge.Data.Repository
{
    public class PhoneRepository : BaseRepository
    {
        public IEnumerable<Phone> GetAll()
        {
            return Phones;
        }

        public IEnumerable<Phone> GetAllByCustomerId(int customerId)
        {
            return Phones.Where(p => p.Customer != null && p.Customer.Id == customerId);
        }

        public Phone GetById(int id)
        {
            return Phones.FirstOrDefault(p => p.Id == id);
        }
    }
}
