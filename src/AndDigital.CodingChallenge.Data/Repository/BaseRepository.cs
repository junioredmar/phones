﻿using System.Collections.Generic;
using AndDigital.CodingChallenge.Data.Models;

namespace AndDigital.CodingChallenge.Data.Repository
{
    public abstract class BaseRepository
    {
        protected IList<Customer> Customers;

        protected IList<Phone> Phones;

        protected BaseRepository()
        {
            SetupRepository();
        }

        private void SetupRepository()
        {
            var edmar = new Customer
            {
                Id = 1,
                Name = "Edmar"
            };
            var philip = new Customer
            {
                Id = 2,
                Name = "Philip"
            };
            var james = new Customer
            {
                Id = 3,
                Name = "James"
            };
            Customers = new List<Customer>
            {
                edmar, philip, james
            };

            var phone1 = new Phone
            {
                Id = 1,
                Customer = edmar,
                IsActive = true,
                Number = "+44 3069 990425"
            };
            var phone2 = new Phone
            {
                Id = 2,
                Customer = edmar,
                IsActive = false,
                Number = "+44 3069 990824"
            };
            var phone3 = new Phone
            {
                Id = 3,
                Customer = philip,
                IsActive = true,
                Number = "+44 3069 990050"
            };
            var phone4 = new Phone
            {
                Id = 4,
                Customer = james,
                IsActive = true,
                Number = "+44 3069 990851"
            };

            Phones = new List<Phone>
            {
                phone1, phone2, phone3, phone4
            };
        }
    }
}
