﻿namespace AndDigital.CodingChallenge.Data.Models
{
    public class Phone
    {
        public int Id { get; set; }

        public Customer Customer { get; set; }

        public string Number { get; set; }

        public bool IsActive { get; set; }
    }
}
