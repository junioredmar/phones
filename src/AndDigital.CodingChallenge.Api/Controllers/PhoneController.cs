﻿using System.Collections.Generic;
using AndDigital.CodingChallenge.Core.Service;
using AndDigital.CodingChallenge.Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace AndDigital.CodingChallenge.Api.Controllers
{
    [Route("api/[controller]")]
    public class PhoneController : Controller
    {
        /// <summary>
        /// Get all phones from every customer
        /// </summary>
        [HttpGet]
        [Route("/api/phones")]
        public IEnumerable<Phone> Get()
        {
            var service = new PhoneService();
            return service.GetAll();
        }

        /// <summary>
        /// Get all phones from a specific customer
        /// </summary>
        /// <param name="customerId">Customer Id to retrive phones</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/phones/{customerId}")]
        public IEnumerable<Phone> Get([FromRoute]int customerId)
        {
            var service = new PhoneService();
            return service.GetAllByCustomerId(customerId);
        }
        
        /// <summary>
        /// Set the phone as active
        /// </summary>
        /// <param name="id">Id of the phone to be activated</param>
        [HttpPut()]
        [Route("/api/phone/{id}/activate")]
        public Phone Put([FromRoute]int id)
        {
            var service = new PhoneService();
            return service.Activate(id);
        }
        
    }
}
