﻿using System.Collections.Generic;
using AndDigital.CodingChallenge.Core.Service;
using AndDigital.CodingChallenge.Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace AndDigital.CodingChallenge.Api.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        /// <summary>
        /// Get all customers
        /// </summary>
        [HttpGet]
        [Route("/api/customers")]
        public IEnumerable<Customer> Get()
        {
            var service = new CustomerService();
            return service.GetAll();
        }
    }
}
