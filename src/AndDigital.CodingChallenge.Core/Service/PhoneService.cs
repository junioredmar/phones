﻿using System.Collections.Generic;
using AndDigital.CodingChallenge.Data.Models;
using AndDigital.CodingChallenge.Data.Repository;

namespace AndDigital.CodingChallenge.Core.Service
{
    public class PhoneService
    {
        private readonly PhoneRepository _repository;

        public PhoneService()
        {
            _repository = new PhoneRepository();
        }

        public IEnumerable<Phone> GetAll()
        {
            return _repository.GetAll();
        }

        public IEnumerable<Phone> GetAllByCustomerId(int customerId)
        {
            return _repository.GetAllByCustomerId(customerId);
        }

        public Phone Activate(int phoneId)
        {
            var phone = _repository.GetById(phoneId);
            if (phone != null)
            {
                phone.IsActive = true;
            }

            // SaveDB
            return phone;
        }
    }
}
