﻿using System.Collections.Generic;
using AndDigital.CodingChallenge.Data.Models;
using AndDigital.CodingChallenge.Data.Repository;

namespace AndDigital.CodingChallenge.Core.Service
{
    public class CustomerService
    {
        private readonly CustomerRepository _repository;

        public CustomerService()
        {
            _repository = new CustomerRepository();
        }

        public IEnumerable<Customer> GetAll()
        {
            return _repository.GetAll();
        }
    }
}
