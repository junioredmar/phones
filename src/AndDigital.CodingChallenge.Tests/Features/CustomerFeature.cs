using AndDigital.CodingChallenge.Core.Service;
using Xunit;

namespace AndDigital.CodingChallenge.Tests.Features
{
    public class CustomerFeature
    {
        private CustomerService _customerService;

        [Fact]
        public void Customers_should_list_all_customers()
        {
            _customerService = new CustomerService();
            var allCustomers = _customerService.GetAll();
            Assert.NotEmpty(allCustomers);
        }
    }
}
