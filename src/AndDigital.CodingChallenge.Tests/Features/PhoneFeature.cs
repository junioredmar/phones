using System.Linq;
using AndDigital.CodingChallenge.Core.Service;
using Xunit;

namespace AndDigital.CodingChallenge.Tests.Features
{
    public class PhoneFeature
    {
        private PhoneService _phoneService;

        [Fact]
        public void Phones_should_list_all_phones()
        {
            _phoneService = new PhoneService();

            var phones = _phoneService.GetAll();

            Assert.NotEmpty(phones);
        }

        [Fact]
        public void Phone_should_list_all_phones_from_specific_customer()
        {
            _phoneService = new PhoneService();

            var phones = _phoneService.GetAllByCustomerId(2);
            var customerPhone = phones.FirstOrDefault();

            Assert.NotNull(customerPhone);
            Assert.NotNull(customerPhone.Customer);
            Assert.Equal("Philip", customerPhone.Customer.Name);
            Assert.Equal("+44 3069 990050", customerPhone.Number);
        }

        [Fact]
        public void Phone_should_not_list_phones_from_non_existent_customer()
        {
            _phoneService = new PhoneService();

            var phones = _phoneService.GetAllByCustomerId(9999);

            Assert.Empty(phones);
        }

        [Fact]
        public void Phone_should_activate_phone_by_id()
        {
            _phoneService = new PhoneService();

            var phone = _phoneService.Activate(2);

            Assert.NotNull(phone);
            Assert.True(phone.IsActive);
        }
    }
}
